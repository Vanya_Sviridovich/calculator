﻿using System;
using System.Text;

namespace Calculator
{
    class Program
    {
        private static string _operationsHistory;

        static void Main(string[] args)
        {
            Launch();
        }

        private static void Launch()
        {
            while (true)
            {
                ShowMenu();

                var input = Console.ReadLine();

                if (input == null || input.ToLower().Trim().Equals("c"))
                    break;

                if (!IsValidValue(input) || string.IsNullOrEmpty(input))
                {
                    DisplayError("Invalid value");
                    continue;
                }

                ChooseOperation(input);
            }
        }

        private static void ChooseOperation(string input)
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.WriteLine("Please enter 2 numbers one by one");

            Object result = 0;

            switch (Int32.Parse(input))
            {
                case 1:
                    result = Summarize(GetNumber(), GetNumber());
                    break;
                case 2:
                    result = Subtract(GetNumber(), GetNumber());
                    break;
                case 3:
                    result = Multiple(GetNumber(), GetNumber());
                    break;
                case 4:
                    result = Divide(GetNumber(), GetNumber());
                    if (result == null)
                        return;
                    break;
                case 5:
                    result = SetMatrices(InitializeMatrix("first"), InitializeMatrix("second"));
                    //if only first initialization has been failed.
                    if (((double[,])result) == null)
                        return;
                    break;
                case 6:
                    DisplayHistory();
                    return;
            }
            
            while (true)
            {
                Console.WriteLine("Would you like to use the sum as first argument? [Yes - 1], [No - 2]");

                if (Int32.TryParse(Console.ReadLine(), out int choice) && choice == 1)
                {
                    Console.Clear();
                    result = UseLastCalculationAsFirstArgument(input, result);
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    break;

                }
                else
                    EraseInvalidValue(Console.CursorLeft, Console.CursorTop - 2);
            }
        }

        private static object UseLastCalculationAsFirstArgument(string input, object obj)
        {
            var operation = Int32.Parse(input);

            if (operation == 1)
            {
                DisplayArgument(obj.ToString());
                return Summarize((double)obj, GetNumber());
            }
            else if (operation == 2)
            {
                DisplayArgument(obj.ToString());
                return Subtract((double)obj, GetNumber());
            }
            else if (operation == 3)
            {
                DisplayArgument(obj.ToString());
                return Multiple((double)obj, GetNumber());
            }
            else if (operation == 4)
            {
                DisplayArgument(obj.ToString());
                return Divide((double)obj, GetNumber());
            }
            else
            {
                DisplayMatrix("First matrix", (double[,]) obj);
                return MultiplyReceivedMatrix((double[,])obj, InitializeMatrix("second"));
            }
        }

        private static object Summarize(double a, double b)
        {
            Console.WriteLine($"{DisplayDashes()}\n{a} + {b} = {a + b}\n{DisplayDashes()}");
            SaveOperation("Addition", $"{a} + {b} = {a + b}");
            return a + b;
        }

        private static object Subtract(double a, double b)
        {
            Console.WriteLine($"{DisplayDashes()}\n{a} - {b} = {a - b}\n{DisplayDashes()}");
            SaveOperation("Subtraction", $"{a} - {b} = {a - b}");
            return a - b;
        }

        private static object Multiple(double a, double b)
        {
            Console.WriteLine($"{DisplayDashes()}\n{a} * {b} = {a * b}\n{DisplayDashes()}");
            SaveOperation("Multiplication", $"{a} * {b} = {a * b}");
            return a * b;
        }

        private static object Divide(double a, double b)
        {
            var zero = 0;

            if (b.Equals(zero))
            {
                DisplayError("Cannot divide by zero");
                return null;
            }

            Console.WriteLine($"{DisplayDashes()}\n{a} / {b} = {a / b}\n{DisplayDashes()}");
            SaveOperation("Division", $"{a} / {b} = {a / b}");
            return a / b;

        }

        private static double [,] SetMatrices(double[,] a, double [,] b)
        {
            if (!IsValidDimension(a, b))
                return null;

            DrawMatrix(a);
            DrawMatrix(b);

            return MultiplyMatrices(a, b);
        }
        private static double[,] InitializeMatrix(string message)
        {
            Console.WriteLine(
                $"{DisplayDashes()}\nSet {message} matrix dimensions entering amount of rows and then columns");
            return new double[Convert.ToInt32(GetNumber()), Convert.ToInt32(GetNumber())];
        }

        private static bool IsValidDimension(double[,] a, double[,] b)
        {
            if (a.GetUpperBound(1) != b.GetUpperBound(0) || a.GetUpperBound(0) + 1 <= 0 || a.GetUpperBound(1) + 1 <= 0 || b.GetUpperBound(0) + 1 <= 0 ||
                b.GetUpperBound(1) + 1 <= 0)
            {
                DisplayError("Invalid properties. Matrices multiplication is forbidden");
                return false;
            }
            return true;
        }

        private static void DrawMatrix(double[,] matrix)
        {
            Console.WriteLine(
                $"{DisplayDashes()}\nFilling the matrix out one by one. The measures of matrix are [{matrix.GetUpperBound(0) + 1}, {matrix.GetUpperBound(1) + 1}]");

            for (int i = 0; i <= matrix.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= matrix.GetUpperBound(1); j++)
                {
                    var cursorPointerHorizontally = (j + 1) * 9;

                    if (Double.TryParse(Console.ReadLine(), out double number))
                    {
                        matrix[i, j] = number;
                        Console.SetCursorPosition(cursorPointerHorizontally, Console.CursorTop - 1);
                    }
                    else
                    {
                        j--;
                        cursorPointerHorizontally = (j + 1) * 9;
                        EraseInvalidValue(cursorPointerHorizontally, Console.CursorTop - 1);
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine(DisplayDashes());
        }
        
        private static double[,] MultiplyMatrices(double[,] firstMatrix, double[,] secondMatrix)
        {

        var multipliedMatrix = new double[Convert.ToInt32(firstMatrix.GetUpperBound(0) + 1),
                Convert.ToInt32(secondMatrix.GetUpperBound(1) + 1)];

            for (int rowFirstMatrix = 0; rowFirstMatrix <= firstMatrix.GetUpperBound(0); rowFirstMatrix++)
            {
                for (int columnSecondMatrix = 0; columnSecondMatrix <= secondMatrix.GetUpperBound(1); columnSecondMatrix++)
                {
                    double result = 0;

                    for (int item = 0; item <= firstMatrix.GetUpperBound(1); item++)
                    {
                        result += firstMatrix[rowFirstMatrix, item] * secondMatrix[item, columnSecondMatrix];
                    }

                    multipliedMatrix[rowFirstMatrix, columnSecondMatrix] = result;
                }
            }
            DisplayMatrix("Multiplied matrix", multipliedMatrix);

            StringBuilder matricesRecording = RecordOperation(new StringBuilder(), firstMatrix, secondMatrix, multipliedMatrix);

            SaveOperation("Matrix Multiplication", matricesRecording.ToString().Trim());

            return multipliedMatrix;
        }

        private static void DisplayMatrix(string message, double[,] array)
        {
            Console.Clear();
            Console.WriteLine($"{DisplayDashes()}\n{message}:");

            for (int i = 0; i <= array.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= array.GetUpperBound(1); j++)
                {
                    Console.WriteLine(array[i, j]);

                    Console.SetCursorPosition((j + 1) * 9, Console.CursorTop - 1);
                }
                Console.WriteLine();
            }
            Console.WriteLine(DisplayDashes());
        }

        private static double[,] MultiplyReceivedMatrix(double[,] receivedMatrix, double[,] b)
        {
            if (!IsValidDimension(receivedMatrix, b))
                return receivedMatrix;

            DrawMatrix(b);

            return MultiplyMatrices(receivedMatrix, b);
        }

        private static StringBuilder RecordOperation(StringBuilder matricesRecording, double[,] firstMatrix, double[,] secondMatrix, double[,] multipliedMatrix)
        {
            matricesRecording = RecordMatrix(firstMatrix, matricesRecording);
            matricesRecording.Append("'multiplied by'\n");
            matricesRecording = RecordMatrix(secondMatrix, matricesRecording);
            matricesRecording.Append("'equals to'\n");
            matricesRecording = RecordMatrix(multipliedMatrix, matricesRecording);
            return matricesRecording;
        }
        private static StringBuilder RecordMatrix(double[,] array, StringBuilder str)
        {
            str.Append('-', 20).Append("\n");

            for (int i = 0; i <= array.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= array.GetUpperBound(1); j++)
                {
                    str.Append(array[i, j]).Append(' ', 9);
                }
                str.Append("\n");
            }
            str.Append('-', 20).Append("\n");
            return str;
        }

        private static void ShowMenu()
        {
            var addition = 1;
            var subtraction = 2;
            var multiplication = 3;
            var division = 4;
            var matrixMultiplication = 5;
            var calculationHistory = 6;
            var exit = "c";

            Console.WriteLine("Choose operation 'digit', according to the specification below");
            Console.WriteLine(
                $"[Sum - {addition}] [Subtraction - {subtraction}] [Multiplication - {multiplication}] [Division - {division}] " +
                $"[Matrix Multiplication - {matrixMultiplication}] [History - {calculationHistory}] [Exit - {exit}]");
            Console.WriteLine();
        }

        private static bool IsValidValue(string input)
        {
            if (Int32.TryParse(input, out int digit) && (digit >= 1 && digit <= 6))
                return true;
            return false;
        }
        private static double GetNumber()
        {
            while (true)
            {
                if (Double.TryParse(Console.ReadLine(), out double number))
                    return number;
                EraseInvalidValue(Console.CursorLeft, Console.CursorTop - 1);
            }
        }

        private static void EraseInvalidValue(int cursorLeft, int cursorTop)
        {
            Console.SetCursorPosition(cursorLeft, Console.CursorTop - 1);
            Console.Write(new String(' ', Console.BufferWidth));
            Console.SetCursorPosition(cursorLeft, cursorTop);

        }

        private static void DisplayArgument(string message)
        {
            Console.WriteLine($"The first argument is: {message}. Enter the other");
        }

        private static string DisplayDashes()
        {
            return "====================";
        }

        private static void SaveOperation(string title, object operation)
        {
            _operationsHistory += $"{DisplayDashes()}\n{title}\n{operation}\n";
        }

        private static void DisplayHistory()
        {
            Console.Clear();
            
            if (_operationsHistory == null)
                Console.WriteLine("No operation has been done yet.\nPress any characters");
            
            else
                Console.WriteLine($"Operations history:\n{_operationsHistory.Trim()}\nPress any characters");
            
            Console.ReadLine();
            Console.Clear();

        }

        private static void DisplayError(string message)
        {
            Console.Clear();
            Console.WriteLine($"{message}. Press anything");
            Console.ReadLine();
            Console.Clear();
        }
    }
}